
const Course = require("../models/course");

module.exports.getAllCourses = (req,res)=>{

    // res.send("This route will get all courses documents");

    Course.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}


module.exports.addCourse = (req,res)=>{

    // console.log(req.body);

let newCourse = new Course({
	
	name: req.body.name,
	description: req.body.description,
	price: req.body.price

  })
// console.log(newCourse);

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getActiveCourses = (req,res) => {


	Course.find({isActive: true})
	.then(result => res.send(result))
    .catch(error => res.send(error))
}


///ACTIVITY

module.exports.getSingleCourse = (req,res)=>{

    

   Course.findById(req.body.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))

}